import org.json.*;

import Graph.Graph;
import Graph.Vertex;

public class Map {
	Graph<Room> map = new Graph<>();
	
	public Map() {
		generateMap();
	}
	
	public Room getRoomById(int roomId) {
		return; //find room by id
	}
	
	public void generateMap() {
		//generate from data
		
		String roomFile = "{\"rooms\":[{\"id\":1,\"text\":\"Vesli jste do mistnosti 1\",\"events\":[{\"type\":\"move\",\"destination\":2},{\"type\":\"move\",\"destination\":3}]},{\"id\":2,\"text\":\"Vesli jste do mistnosti 2\",\"events\":[{\"type\":\"move\",\"destination\":1}]},{\"id\":3,\"text\":\"Vesli jste do mistnosti 3\",\"events\":[{\"type\":\"move\",\"destination\":1}]}]}";
		
		JSONObject obj = new JSONObject(roomFile);
		
		//String pageName = obj.getJSONObject("pageInfo").getString("pageName");
		
		JSONArray rooms = obj.getJSONArray("rooms");
		for (int roomId = 0; roomId < rooms.length(); roomId++) {
		    //String post_id = rooms.getJSONObject(i).getString("post_id");
			Room room = new Room(rooms.getJSONObject(roomId).getInt("id"), rooms.getJSONObject(roomId).getString("text"));
			
			JSONArray events = rooms.getJSONObject(roomId).getJSONArray("events");
			
			for (int eventId = 0; eventId < events.length(); eventId++) {
				String eventType = events.getJSONObject(eventId).getString("type");
				
				if (eventType == "move") {
					int destinationId = events.getJSONObject(eventId).getInt("destination");
					
					room.addEvent("Presun se do mistnosti" + destinationId, Global.actionController.readAction("move"), Global.player, destinationId);
				}
			}
			
		    map.addVertex(new Vertex<Room>(room));
		}
	}
}
