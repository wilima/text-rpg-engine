import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

import Graph.Graph;
import Graph.Vertex;

/**
 * @author tomasmikula
 *
 */
public class GameState {
	private Player player = new Player();
	private Map map = new Map();
	
	public GameState() {
	}
	
	public Action prompt(Scanner console) {
		//print actual game state
		
		System.out.println("Muzes jenom exit");
		
		return Global.actionController.readAction(console.next());
	}
	
	public void moveGameState(Action action) {
		action.invoke();
	}
	
	public Map getMap() {
		return this.map;
	}

}
