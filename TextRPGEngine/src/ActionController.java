
import java.util.HashMap;
import java.util.Map;

public class ActionController {
	private Map<String, Action> actions = new HashMap<>();
	
	public ActionController() {
		registerAction("exit", new Action() {
			
			@Override
			public void invoke(Object... objects) {
				Global.gameIsRunning = false;
			}
		});

		
		registerAction("move", new Action() {
			
			@Override
			public void invoke() {
				Player player = (Player) objects[0];
				int roomId = (int) objects[1];
				
				player.move(Global.gameState.getMap().getRoomById(roomId));
			}
		});
	}
	
	public void registerAction(String string, Action action) {
		actions.put(string, action);
	}
	
	public Action readAction(String string) {
		return actions.get(string);
	}

}
