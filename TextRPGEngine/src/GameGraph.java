import Graph.Graph;


public class GameGraph {
	private Graph<Room> game = new Graph<>();

	public Graph<Room> getGame() {
		return game;
	}

	public void setGame(Graph<Room> game) {
		this.game = game;
	}
	
	public void generateGameGraph() {
		
	}
}
