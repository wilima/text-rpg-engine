import java.util.Scanner;

/**
 * Class for Main GameLoop
 */

/**
 * @author tomasmikula
 *
 */
public final class GameLoop {	
	
	//Main game loop
	public static void gameLoop() {
		Scanner console = new Scanner(System.in);
		
		
		while (Global.gameIsRunning) {
			Action action = Global.gameState.prompt(console);
			Global.gameState.moveGameState(action);
		}
	}
	
}