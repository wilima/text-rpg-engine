import java.util.HashMap;
import java.util.Map;


public class Room {
	private int id;
	private String roomText;
	private Map<String, Action> events = new HashMap<>();
	private Map<String, Object[]> args = new HashMap<>();
	
	public Room(int id, String roomText) {
		this.roomText = roomText;
	}
	
	public int getId() {
		return id;
	}

	public String getRoomText() {
		return roomText;
	}

	public void addEvent(String text, Action action, Object... objects) {
		this.events.put(text, action);
		this.args.put(text, objects);
	}
}
