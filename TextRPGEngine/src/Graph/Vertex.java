package Graph;

import java.util.HashMap;
import java.util.Map;

public class Vertex<T> {
	private T content;
	private Map<Vertex<T>, Object> connected = connected = new HashMap<>();
	
	public Vertex(T content) {
		this.content = content;
	}
}
