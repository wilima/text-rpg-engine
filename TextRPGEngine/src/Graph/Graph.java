package Graph;

import java.util.HashSet;
import java.util.Set;

public class Graph<T> {
	private Set<Vertex<T>> vertexes = new HashSet<>();
	
	public Graph() {
	}

	public void addVertex(Vertex<T> vertex) {
		this.vertexes.add(vertex);
	}
}
