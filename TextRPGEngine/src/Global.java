
/**
 * Global variables
 */

/**
 * @author tomasmikula
 *
 */

public class Global {
	public static boolean gameIsRunning = true;
	public static ActionController actionController = new ActionController();
	public static GameState gameState = new GameState();
}
